package server

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/middlewares"

	"git.sipher.co/sproutways/external/api/account/controllers"
)

// API : _ => http.Handler
func API() http.Handler {
	time.Local = time.UTC
	//gin.SetMode(gin.ReleaseMode)

	// gin
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, POST",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     false,
		ValidateHeaders: false,
	}))

	// v1
	v1 := router.Group("/v1")

	v1.Use(middlewares.AuthenticationCheck)
	v1.POST("/account/confirm", controllers.PostAccountConfirm)
	v1.POST("/account/confirm/resend", controllers.PostAccountConfirmResend)
	v1.POST("/email/confirm", controllers.PostEmailConfirm)
	v1.POST("/email/update", controllers.PostEmailUpdate)
	v1.POST("/username/update", controllers.PostUsernameUpdate)
	v1.POST("/password/set", controllers.PostPasswordSet)
	v1.POST("/password/update", controllers.PostPasswordUpdate)

	v1.GET("/profile/headline", controllers.GetProfileHeadline)
	v1.GET("/profile/story", controllers.GetProfileStory)
	v1.GET("/profile/avatar", controllers.GetProfileAvatar)

	v1.POST("/profile/headline/update", controllers.PostProfileHeadlineUpdate)
	v1.POST("/profile/story/update", controllers.PostProfileStoryUpdate)
	v1.POST("/profile/avatar/upload", controllers.PostProfileAvatarUpload)

	return router
}

// Server : &http.Server
var Server = &http.Server{
	Addr:         configs.GetServer().APIAccount.Port,
	Handler:      API(),
	ReadTimeout:  5 * time.Second,
	WriteTimeout: 10 * time.Second,
}
