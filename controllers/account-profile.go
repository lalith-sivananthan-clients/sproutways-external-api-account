package controllers

import (
	"io"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/leebenson/conform"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/codes"
	"git.sipher.co/sproutways/external/shared/services/media"
	"git.sipher.co/sproutways/external/shared/services/validations"
)

type (
	requestPostProfileAvatarUpload struct {
		XOffset int  `form:"XOffset" conform:"trim"`
		YOffset int  `form:"YOffset" conform:"trim"`
		Width   uint `form:"Width"   conform:"trim"`
		Height  uint `form:"Height"  conform:"trim"`
	}

	requestPostProfileHeadlineUpdate struct {
		Headline string `form:"Headline" conform:"trim" binding:"required"`
	}

	requestPostProfileStoryUpdate struct {
		Story string `form:"Story" conform:"trim" binding:"required"`
	}
)

// GET : v#/profile/headline
func GetProfileHeadline(c *gin.Context) {
	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	c.JSON(200, Response(true, codes.ResultSuccess, account.Profile.Headline))
}

// GET : v#/profile/story
func GetProfileStory(c *gin.Context) {
	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	c.JSON(200, Response(true, codes.ResultSuccess, account.Profile.Story))
}

// GET : v#/profile/avatar
func GetProfileAvatar(c *gin.Context) {
	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	c.JSON(200, Response(true, codes.ResultSuccess, account.Profile.Avatar.MediaID.Hex()))
}

// POST : v#/profile/headline/update
func PostProfileHeadlineUpdate(c *gin.Context) {
	// parameters -> bind
	var params requestPostProfileHeadlineUpdate

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> update
	update := bson.M{
		"$set": bson.M{
			"pr.hl": params.Headline,
			"wh.ua": time.Now(),
		},
	}

	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/profile/story/update
func PostProfileStoryUpdate(c *gin.Context) {
	// parameters -> bind
	var params requestPostProfileStoryUpdate

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> update
	update := bson.M{
		"$set": bson.M{
			"pr.st": params.Story,
			"wh.ua": time.Now(),
		},
	}

	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/profile/avatar/upload
func PostProfileAvatarUpload(c *gin.Context) {
	// parameters -> bind
	var params requestPostProfileAvatarUpload

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// file : get
	file, _, err := c.Request.FormFile("File")
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileMissing))
		c.Abort()
		return
	}

	// file : get -> content-type
	buffer := make([]byte, 512)
	_, err = file.Read(buffer)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileInvalid))
		c.Abort()
		return
	}

	file.Seek(0, 0)

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : media -> data
	image := &models.MediaImage{
		ID:        bson.NewObjectId(),
		AccountID: account.ID,
		Mime:      "",
		Status:    "processing",
		Type:      "image",
		For:       "avatar",
		Location:  "storage/images/",
		Crop: models.MediaImageCrop{
			XOffset: params.XOffset,
			YOffset: params.YOffset,
			Width:   params.Width,
			Height:  params.Height,
		},
		When: models.WhenNew(),
	}

	var path string
	var extension string

	switch http.DetectContentType(buffer) {
	case "image/jpeg":
		extension = "jpg"
		image.Mime = "image/jpeg"
		path = configs.PathGatewayMedia + image.Location + image.ID.Hex() + "_temp.jpg"
	case "image/png":
		extension = "png"
		image.Mime = "image/png"
		path = configs.PathGatewayMedia + image.Location + image.ID.Hex() + "_temp.png"
	default:
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileInvalid))
		c.Abort()
		return
	}

	// file : create
	destination, err := os.Create(path)
	defer destination.Close()
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileCreate))
		c.Abort()
		return
	}

	// file : save
	_, err = io.Copy(destination, file)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileSave))
		c.Abort()
		return
	}

	// db : refresh
	models.DBStorageSessionMedia.Refresh()

	// db : media -> create
	err = models.DBStorageCollectionMedia.Insert(image)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelStorageMediaCreate))
		c.Abort()
		return
	}

	err = media.CopyPaste(configs.PathGatewayMedia+image.Location, image.ID.Hex()+"_temp."+extension, configs.PathGatewayMedia+image.Location, image.ID.Hex())
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileCreate))
		c.Abort()
		return
	}

	err = media.Crop(configs.PathGatewayMedia+image.Location, image.ID.Hex(), image.Crop.XOffset, image.Crop.YOffset, image.Crop.Width, image.Crop.Height)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileCreate))
		c.Abort()
		return
	}

	err = media.Resize(configs.PathGatewayMedia+image.Location, image.ID.Hex(), 500, 500)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorFileCreate))
		c.Abort()
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> update
	update := bson.M{
		"$set": bson.M{
			"pr.av._mid": image.ID,
			"pr.av.st":   "set",
		},
	}

	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, image.ID.Hex()))
}
