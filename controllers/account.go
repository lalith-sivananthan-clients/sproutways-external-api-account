package controllers

import (
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/leebenson/conform"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
	"git.sipher.co/sproutways/external/shared/services/codes"
	"git.sipher.co/sproutways/external/shared/services/helpers"
	"git.sipher.co/sproutways/external/shared/services/validations"

	"git.sipher.co/sproutways/external/daemon/queue/dispatchers"
)

type (
	requestPostAccountConfirm struct {
		Token string `form:"Token" conform:"trim" binding:"required" validate:"required,tokenEmail"`
	}

	requestPostEmailUnique struct {
		Email string `form:"Email" conform:"trim,lowercase,email" binding:"required" validate:"required,email,emailUnique"`
	}

	requestPostEmailConfirm struct {
		Token string `form:"Token" conform:"trim" binding:"required" validate:"required,tokenEmail"`
	}

	requestPostnameUpdate struct {
		Username string `form:"Username" conform:"trim" binding:"required" validate:"required"`
	}

	requestPostPasswordSet struct {
		Password        string `form:"Password"        conform:"trim" binding:"required" validate:"required,min=6,max=64,eqfield=PasswordConfirm"`
		PasswordConfirm string `form:"PasswordConfirm" conform:"trim" binding:"required" validate:"required,min=6,max=64"`
	}

	requestPostPasswordUpdate struct {
		Password           string `form:"Password"           conform:"trim" binding:"required" validate:"required"`
		PasswordNew        string `form:"PasswordNew"        conform:"trim" binding:"required" validate:"required,min=6,max=64,eqfield=PasswordNewConfirm"`
		PasswordNewConfirm string `form:"PasswordNewConfirm" conform:"trim" binding:"required" validate:"required,min=6,max=64"`
	}
)

// POST : v#/account/confirm
func PostAccountConfirm(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountConfirm

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// token
	token := strings.Split(params.Token, "-0-")
	accountID := bson.ObjectIdHex(token[0])
	accountEmailID := bson.ObjectIdHex(token[1])

	if account.ID != accountID {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorTokenInvalid))
		c.Abort()
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()
	models.DBAuthenticationSessionAccountEmails.Refresh()

	// db : account-email -> get
	count, err := models.DBAuthenticationCollectionAccountEmails.FindId(accountEmailID).Count()
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
		c.Abort()
		return
	}

	// db : account-email -> get
	accountEmail := &models.AccountEmail{}

	err = models.DBAuthenticationCollectionAccountEmails.FindId(accountEmailID).One(&accountEmail)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
		c.Abort()
		return
	}

	if count == 0 || account.ID != accountEmail.AccountID {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorTokenInvalid))
		c.Abort()
		return
	}

	// db : account -> data
	update := bson.M{
		"$set": bson.M{
			"co":    true,
			"wh.ua": time.Now(),
		},
	}

	// db : account -> update
	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	// db : account-email -> delete
	err = models.DBAuthenticationCollectionAccountEmails.RemoveId(accountEmailID)
	if err != nil {
		// TODO
		// queue -> db : account-email
		// delete : id -> accountEmailID
	}

	err = models.DBAuthenticationCollectionAccounts.FindId(account.ID).One(&account)

	if err == nil {
		// queue -> email
		data := make([]interface{}, 1)
		data[0] = account

		payload := &dispatchers.PayloadEmail{
			Template: dispatchers.TemplateAccountConfirmed,
			Data:     data,
		}

		dispatchers.Email(payload)
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/account/confirm/resend
func PostAccountConfirmResend(c *gin.Context) {
	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	if account.Confirmed == true {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorAccountAlreadyConfirmed))
		c.Abort()
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccountEmails.Refresh()

	// db : account-email -> query
	query := bson.M{
		"_aid": account.ID,
		"em":   account.Email,
	}

	// db : account-email -> get
	accountEmail := &models.AccountEmail{}

	count, err := models.DBAuthenticationCollectionAccountEmails.Find(query).Count()
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
		c.Abort()
		return
	}

	if count == 0 {
		// db : account-email -> data
		accountEmail = &models.AccountEmail{
			ID:        bson.NewObjectId(),
			AccountID: account.ID,
			Email:     account.Email,
			Token:     helpers.CreateToken(64),
			When:      models.WhenTokenNew(),
		}

		// db : account-email -> create
		err = models.DBAuthenticationCollectionAccountEmails.Insert(&accountEmail)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailCreate))
			c.Abort()
			return
		}
	} else {
		// db : account-email -> data
		update := bson.M{
			"$set": bson.M{
				"wh.ea": time.Now().Add(time.Minute * 60 * 24 * 7),
				"wh.ua": time.Now(),
			},
		}

		// db : account-email -> update
		err = models.DBAuthenticationCollectionAccountEmails.Update(query, update)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailUpdate))
			c.Abort()
			return
		}

		// db : account-email -> get
		err = models.DBAuthenticationCollectionAccountEmails.Find(query).One(&accountEmail)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
			c.Abort()
			return
		}
	}

	// queue -> email
	data := make([]interface{}, 2)
	data[0] = account
	data[1] = *accountEmail

	payload := &dispatchers.PayloadEmail{
		Template: dispatchers.TemplateAccountConfirmResend,
		Data:     data,
	}

	dispatchers.Email(payload)

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/email/update
func PostEmailUpdate(c *gin.Context) {
	// parameters -> bind
	var params requestPostEmailUnique

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBAuthenticationSessionAccountEmails.Refresh()

	// db : account-email -> query
	accountEmail := &models.AccountEmail{}

	query := bson.M{
		"_aid": account.ID,
		"em":   params.Email,
	}

	count, err := models.DBAuthenticationCollectionAccountEmails.Find(query).Count()
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
		c.Abort()
		return
	}

	if count == 0 {
		// db : account-email -> data
		accountEmail = &models.AccountEmail{
			ID:        bson.NewObjectId(),
			AccountID: account.ID,
			Email:     params.Email,
			Token:     helpers.CreateToken(64),
		}

		// db : account-email -> create
		err = models.DBAuthenticationCollectionAccountEmails.Insert(&accountEmail)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailCreate))
			c.Abort()
			return
		}
	} else {
		// db : account-email -> data
		update := bson.M{
			"$set": bson.M{
				"wh.ea": time.Now().Add(time.Minute * 60 * 24 * 7),
				"wh.ua": time.Now(),
			},
		}

		// db : account-email -> update
		err = models.DBAuthenticationCollectionAccountEmails.Update(query, update)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailUpdate))
			c.Abort()
			return
		}

		// db : account-email -> get
		err = models.DBAuthenticationCollectionAccountEmails.Find(query).One(&accountEmail)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
			c.Abort()
			return
		}
	}

	// queue -> email
	data := make([]interface{}, 2)
	data[0] = account
	data[1] = *accountEmail

	payload := &dispatchers.PayloadEmail{
		Template: dispatchers.TemplateAccountEmailUpdate,
		Data:     data,
	}

	dispatchers.Email(payload)

	c.JSON(200, Response(true, codes.ResultSuccess, true))

}

// POST : v#/email/confirm
func PostEmailConfirm(c *gin.Context) {
	// parameters -> bind
	var params requestPostEmailConfirm

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()
	models.DBAuthenticationSessionAccountEmails.Refresh()

	// token
	token := strings.Split(params.Token, "-0-")
	accountID := bson.ObjectIdHex(token[0])
	accountEmailID := bson.ObjectIdHex(token[1])

	if account.ID != accountID {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorTokenInvalid))
		c.Abort()
		return
	}

	// db : account-email -> count
	count, err := models.DBAuthenticationCollectionAccountEmails.FindId(accountEmailID).Count()
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
		c.Abort()
		return
	}

	if count == 0 {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorTokenInvalid))
		c.Abort()
		return
	}

	// db : account-email -> count
	accountEmail := &models.AccountEmail{}

	err = models.DBAuthenticationCollectionAccountEmails.FindId(accountEmailID).One(&accountEmail)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailGet))
		c.Abort()
		return
	}

	if account.ID != accountEmail.AccountID {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorTokenInvalid))
		c.Abort()
		return
	}

	// db : account -> query
	update := bson.M{
		"$set": bson.M{
			"em":    accountEmail.Email,
			"wh.ua": time.Now(),
		},
	}

	// db : account -> update
	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	// db : account-email -> query
	query := bson.M{
		"_aid": account.ID,
	}

	// db : account-email -> delete
	err = models.DBAuthenticationCollectionAccountEmails.Remove(query)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountEmailDelete))
		c.Abort()
		return
	}

	// db : account -> get
	err = models.DBAuthenticationCollectionAccounts.FindId(account.ID).One(&account)

	if err == nil {
		if account.Confirmed == false {
			// db : account-email -> data
			accountEmail = &models.AccountEmail{
				ID:        bson.NewObjectId(),
				AccountID: account.ID,
				Email:     account.Email,
				Token:     helpers.CreateToken(64),
				When:      models.WhenTokenNew(),
			}

			// db : account-email -> create
			err = models.DBAuthenticationCollectionAccountEmails.Insert(&accountEmail)

			if err == nil {
				// queue -> email
				data := make([]interface{}, 2)
				data[0] = account
				data[1] = *accountEmail

				payload := &dispatchers.PayloadEmail{
					Template: dispatchers.TemplateAccountConfirmResend,
					Data:     data,
				}

				dispatchers.Email(payload)
			}
		}

		// queue -> email
		data := make([]interface{}, 1)
		data[0] = account

		payload := &dispatchers.PayloadEmail{
			Template: dispatchers.TemplateAccountEmailConfirmed,
			Data:     data,
		}

		dispatchers.Email(payload)
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/username/update
func PostUsernameUpdate(c *gin.Context) {
	// parameters -> bind
	var params requestPostnameUpdate

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> update
	update := bson.M{
		"$set": bson.M{
			"un":    params.Username,
			"wh.ua": time.Now(),
		},
	}

	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/password/set
func PostPasswordSet(c *gin.Context) {
	// parameters -> bind
	var params requestPostPasswordSet

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> update
	update := bson.M{
		"$set": bson.M{
			"pw":    authentications.CreatePassword(params.Password),
			"ps":    true,
			"wh.ua": time.Now(),
		},
	}

	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	// queue -> email
	data := make([]interface{}, 1)
	data[0] = account

	payload := &dispatchers.PayloadEmail{
		Template: dispatchers.TemplateAccountPasswordChanged,
		Data:     data,
	}

	dispatchers.Email(payload)

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST : v#/password/update
func PostPasswordUpdate(c *gin.Context) {
	// parameters -> bind
	var params requestPostPasswordUpdate

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// get -> account
	accountGet, accountExist := c.Get("account")

	if accountExist == false {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountNotSet))
		c.Abort()
		return
	}

	account := accountGet.(models.Account)

	// authentication -> check
	authenticatePassword := authentications.Check(account.Password, params.Password)

	if authenticatePassword != true {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorAccountInvalidPassword))
		c.Abort()
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> update
	update := bson.M{
		"$set": bson.M{
			"pw":    authentications.CreatePassword(params.PasswordNew),
			"wh.ua": time.Now(),
		},
	}

	err = models.DBAuthenticationCollectionAccounts.UpdateId(account.ID, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	// queue -> email
	data := make([]interface{}, 1)
	data[0] = account

	payload := &dispatchers.PayloadEmail{
		Template: dispatchers.TemplateAccountPasswordChanged,
		Data:     data,
	}

	dispatchers.Email(payload)

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}
