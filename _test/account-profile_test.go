package test

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"github.com/davecgh/go-spew/spew"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/controllers"
)

func TestPostProfileAvatarUpload(t *testing.T) {

	var result controllers.ResponseStruct

	uri := "http://" + configs.GetServer().APIAccount.IP + configs.GetServer().APIAccount.Port + "/v1/profile/avatar/upload"
	path := configs.PathAPIAccount + "_test/account-profile_test.jpg"

	request, err := uploadRequest(uri, "upload", path)

	if err != nil {
		log.Print("UploadRequest", err)
		return
	}

	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		log.Print("client.Do", err, "\n")
		return
	}

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Print("ioutil.ReadAll", err, "\n")
		response.Body.Close()
		return
	}

	response.Body.Close()
	err = json.Unmarshal(body, &result)

	spew.Dump(result)

	return
}

func uploadRequest(uri string, name, path string) (*http.Request, error) {

	file, err := os.Open(path)

	if err != nil {
		return nil, err
	}

	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	part, err := writer.CreateFormFile(name, filepath.Base(path))

	if err != nil {
		return nil, err
	}

	_, err = io.Copy(part, file)

	err = writer.Close()

	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest("POST", uri, body)
	request.Header.Add("Content-Type", writer.FormDataContentType())

	spew.Dump("boundry", writer.Boundary())

	return request, err
}
